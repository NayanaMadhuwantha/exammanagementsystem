<?php


namespace App\Helpers;


class Helper
{
    public static function success($message,$objects=null,$objectName=null)
    {
        $response =  [
            'status'=>'success',
            'message'=>$message
        ];

        if($objects){
            $response = array_merge($response,[$objectName=>$objects]);
        }
        return response()->json($response);
    }
    public static function error($message)
    {
        return response()->json([
            'status'=>'error',
            'message'=>$message
        ]);
    }
    public static function exeption(\Exception $exception)
    {
        return response()->json([
            'status'=>'error',
            'message'=>$exception->getMessage()
        ]);
    }
}
