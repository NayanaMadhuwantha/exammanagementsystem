<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
        $exams = Exam::all();
        return view('Admin.index')->with('exams',$exams);
    }
    public function exam($examId){
        $exams = Exam::all();
        $selectedExam = Exam::findOrFail($examId);
        $users = $selectedExam->users;
        return view('Admin.exam')->with('exams',$exams)->with('users',$users)
            ->with('selectedExam',$selectedExam);
    }

    public function student($id){
        $exams = Exam::all();
        $student = User::findOrFail($id);
        $students_exams = $student->exams;
        return view('Admin.student')->with('exams',$exams)->with('student',$student)
            ->with('students_exams',$students_exams);
    }
}
