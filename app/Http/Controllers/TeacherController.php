<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\User;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function index(){
        $students = User::all()->where('role','=','Student');
        return view('Teacher.index')->with('students',$students);
    }
    public function student($id){
        $students = User::all()->where('role','=','Student');
        $selectedStudent = User::findOrFail($id);
        $students_exams = $selectedStudent->exams->sort();
        return view('Teacher.student')->with('students',$students)->with('selectedStudent',$selectedStudent)
            ->with('students_exams',$students_exams);;
    }
}
