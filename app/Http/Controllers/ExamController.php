<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Exam;
use App\Models\Marks;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\True_;
use function PHPUnit\Framework\isEmpty;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Exam[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            if (!Auth::user()->isAdmin()) {
                return Helper::error("You dont have permission to view exams");
            }
            return Exam::paginate(20);
        }
        catch (\Exception $e){
            return Helper::exeption($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            if (!Auth::user()->isAdmin()){
                return Helper::error("You dont have permission to add an exam");
            }

            $validator = Validator::make($request->all(), [
                'course_code' => 'required',
                'name' => 'required',
                'date' => 'required|date',
                'start_time' => 'required|date_format:H:i',
                'end_time' => 'required|after:start_time',
            ]);

            if ($validator->fails()) {
                return $validator->errors();
            }

            $exam = Exam::create([
                'course_code'=>$request->get('course_code'),
                'name'=>$request->get('name'),
                'date'=>$request->get('date'),
                'start_time'=>$request->get('start_time'),
                'end_time'=>$request->get('end_time'),
            ]);

            if ($exam){
                return Helper::success("Exam created successfully",$exam,"exam");
            }
            else{
                return Helper::error("Exam not created");
            }
        }
        catch (\Exception $e){
            return Helper::exeption($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            if (!Auth::user()->isAdmin()) {
                return Helper::error("You dont have permission to view exams");
            }
            return Exam::findOrFail($id);
        }
        catch (\Exception $e){
            return Helper::exeption($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            if (!Auth::user()->isAdmin()){
                return Helper::error("You dont have permission to modify an exam");
            }

            $validator = Validator::make($request->all(), [
                'course_code' => 'required',
                'name' => 'required',
                'date' => 'required|date',
                'start_time' => 'required|date_format:H:i',
                'end_time' => 'required|after:start_time',
            ]);

            if ($validator->fails()) {
                return $validator->errors();
            }

            $exam = Exam::findOrFail($id);

            $exam->course_code = $request->get('course_code');
            $exam->name = $request->get('name');
            $exam->date = $request->get('date');
            $exam->start_time = $request->get('start_time');
            $exam->end_time = $request->get('end_time');

            if ($exam->save()){
                return Helper::success("Exam updated successfully",$exam,"exam");
            }
            else{
                return Helper::error("Exam not updated");
            }
        }
        catch (\Exception $e){
            return Helper::exeption($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            if (!Auth::user()->isAdmin()){
                return Helper::error("You dont have permission to delete an exam");
            }
            $exam = Exam::findOrFail($id);
            if ($exam->delete()){
                return Helper::success("Exam {$id} deleted successfully");
            }
            else{
                return Helper::error("Exam not updated");
            }
        }
        catch (\Exception $e){
            return Helper::exeption($e);
        }
    }

    public function assignExamToStudent(Request $request){
        try {
            if (!Auth::user()->isAdmin()){
                return Helper::error("You dont have permission to assign exams");
            }
            $validator = Validator::make($request->all(), [
                'student_userId' => 'required|integer',
                'exam_id' => 'required|integer',
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }
            $student_userId = $request->get('student_userId');
            $exam_id = $request->get('exam_id');
            $user = User::findOrFail($student_userId);

            if ($user->role == "Student"){
                $exam = Exam::findOrFail($exam_id);
                $alreadyAdded = false;
                foreach($user->exams as $existingExams){
                    if ($existingExams->id == $exam->id){
                        $alreadyAdded = true;
                    }
                }
                if (!$alreadyAdded){
                    $user->exams()->attach([
                        $exam_id
                    ]);
                    return Helper::success("Exam added successfully",$user,"user");
                }
                else{
                    return Helper::error("Exam (id: {$exam_id}) is already added to student (id: {$student_userId})");
                }
            }
            else{
                return Helper::error("User (id: {$student_userId}) is not a student");
            }
        }
        catch (\Exception $e){
            return Helper::exeption($e);
        }
    }

    public function addMarks(Request $request){
        try {
            if (!Auth::user()->isAdmin()){
                return Helper::error("You dont have permission to delete an exam");
            }
            $validator = Validator::make($request->all(), [
                'student_userId' => 'required|integer',
                'exam_id' => 'required|integer',
                'marks' => 'required|numeric|min:0|max:100'
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }
            $student_userId = $request->get('student_userId');
            $exam_id = $request->get('exam_id');
            $exam_marks = $request->get('marks');
            $user = User::findOrFail($student_userId);

            if ($user->role == "Student"){
                if (count($user->exams->where('id','=',$exam_id))==0){
                    return Helper::error("Exam {$exam_id} is not assigned to student {$student_userId}");
                }

                if(!count($user->marks->where('exam_id','=',$exam_id))){
                    $exam = Exam::findOrFail($exam_id);
                    $marks = Marks::create([
                        'marks' => $exam_marks
                    ]);
                    $user->marks()->save($marks);
                    $exam->marks()->save($marks);
                    return Helper::success("Marks added successfully");
                }
                else{
                    return Helper::error("Marks already added");
                }
            }
            else{
                return Helper::error("User (id: {$student_userId}) is not a student");
            }

        }
        catch (\Exception $e){
            return Helper::exeption($e);
        }
    }
}
