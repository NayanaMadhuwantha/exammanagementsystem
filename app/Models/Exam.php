<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_code',
        'name',
        'date',
        'start_time',
        'end_time'
    ];

    public function users(){
        return $this->belongsToMany(User::class,'user_exams');
    }

    public function marks(){
        return $this->hasMany(Marks::class);
    }
}
