<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::get('/home', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/admin',[\App\Http\Controllers\AdminController::class,'index'])->name('admin')->middleware('admin');
Route::get('/admin/{examId}',[\App\Http\Controllers\AdminController::class,'exam'])->middleware('admin');
Route::get('/admin/student/{id}',[\App\Http\Controllers\AdminController::class,'student'])->middleware('admin');
Route::get('/teacher',[\App\Http\Controllers\TeacherController::class,'index'])->name('teacher')->middleware('teacher');
Route::get('/teacher/student/{id}',[\App\Http\Controllers\TeacherController::class,'student'])->middleware('teacher');
Route::get('/student',[\App\Http\Controllers\StudentController::class,'index'])->name('student')->middleware('student');
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
