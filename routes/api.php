<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', [\App\Http\Controllers\UserController::class,'register']);
Route::post('login', [\App\Http\Controllers\UserController::class,'authenticate']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', [\App\Http\Controllers\UserController::class,'getAuthenticatedUser']);
    Route::apiResource('exam', \App\Http\Controllers\ExamController::class);
    Route::post('assignexam', [\App\Http\Controllers\ExamController::class,'assignExamToStudent']);
    Route::post('addmarks', [\App\Http\Controllers\ExamController::class,'addMarks']);
});
