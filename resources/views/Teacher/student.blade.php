@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Students') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <div class="container-fluid">

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div style="color: #14660c">Exams</div>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th scope="col">Student ID</th>
                                                <th scope="col">Email</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($students as $student)
                                                <tr onclick="window.location.href = '{{\Illuminate\Support\Facades\URL::to('/teacher/student/'.$student->id)}}'">
                                                    <th scope="row">{{$student->id}}</th>
                                                    <td>{{$student->email}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-sm-9">
                                        <div style="color: #14660c">Student</div>
                                        <table class="table table-hover">
                                            <tbody>
                                            <tr>
                                                <td>ID</td>
                                                <td>{{$selectedStudent->id}}</td>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td>{{$selectedStudent->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Email Address</td>
                                                <td>{{$selectedStudent->email}}</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <div style="color: #14660c">Assigned exams</div>
                                        <table class="table table-hover">
                                            <thead>
                                            <thead>
                                            <tr>
                                                <th scope="col">ID</th>
                                                <th scope="col">Course Code</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Marks</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($students_exams as $students_exam)
                                                @isset($students_exam->marks->where('user_id','',$selectedStudent->id)->first()->marks)
                                                <tr>
                                                    <th scope="row">{{$students_exam->id}}</th>
                                                    <td>{{$students_exam->course_code}}</td>
                                                    <td>{{$students_exam->name}}</td>
                                                    <td>{{$students_exam->marks->where('user_id','',$selectedStudent->id)->first()->marks}}</td>
                                                </tr>
                                                @endisset
                                            @endforeach
                                            </tbody>
                                            </thead>
                                        </table>
                                    </div>

                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
