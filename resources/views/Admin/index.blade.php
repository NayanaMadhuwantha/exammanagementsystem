@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Exams') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div style="color: #14660c">Exams</div>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th scope="col">Exam ID</th>
                                            <th scope="col">Course Code</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($exams as $exam)
                                            <tr onclick="window.location.href = '{{\Illuminate\Support\Facades\URL::to('/admin/'.$exam->id)}}'">
                                                <th scope="row">{{$exam->id}}</th>
                                                <td>{{$exam->course_code}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-9" style="text-align: center; font-size: 20px; margin-top: 100px;color: #8f939a;">
                                    <div style="margin: auto">Select an exam</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
