@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Exams') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-3">
                                    <div style="color: #14660c">Exams</div>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th scope="col">Exam ID</th>
                                            <th scope="col">Course Code</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($exams as $exam)
                                            <tr onclick="window.location.href = '{{\Illuminate\Support\Facades\URL::to('/admin/'.$exam->id)}}'">
                                                <th scope="row">{{$exam->id}}</th>
                                                <td>{{$exam->course_code}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-sm-9">
                                    <div style="color: #14660c">{{$selectedExam->course_code}}</div>
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <td>ID</td>
                                            <td>{{$selectedExam->id}}</td>
                                        </tr>
                                        <tr>
                                            <td>Course Code</td>
                                            <td>{{$selectedExam->course_code}}</td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td>{{$selectedExam->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Date</td>
                                            <td>{{$selectedExam->date}}</td>
                                        </tr>
                                        <tr>
                                            <td>Start time</td>
                                            <td>{{$selectedExam->start_time}}</td>
                                        </tr>
                                        <tr>
                                            <td>End time</td>
                                            <td>{{$selectedExam->end_time}}</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div style="color: #14660c">Students</div>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">E-mail</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr onclick="window.location.href = '{{\Illuminate\Support\Facades\URL::to('/admin/student/'.$user->id)}}'">
                                                <th scope="row">{{$user->id}}</th>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
